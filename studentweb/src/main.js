import { createApp } from 'vue'
import App from './App.vue'
import PrimeVue from 'primevue/config'
import Dialog from 'primevue/dialog'
import InputText from 'primevue/inputtext'
import Button from 'primevue/button'
import Card from 'primevue/card'
import Toast from "primevue/toast";
import ToastService from "primevue/toastservice";
import DataTable from 'primevue/datatable';
import Column from 'primevue/column';
import ColumnGroup from 'primevue/columngroup'; 

import axios from 'axios'
import VueAxios from 'vue-axios'

import 'primevue/resources/themes/saga-blue/theme.css'       //theme
import 'primevue/resources/primevue.min.css'                 //core css
import 'primeicons/primeicons.css'                           //icons

const app =  createApp(App)
app.use(PrimeVue);
app.use(VueAxios, axios)
app.use(ToastService)
app.provide('axios', app.config.globalProperties.axios) 
app.component('Dialog', Dialog);
app.component('InputText', InputText);
app.component('Button', Button);
app.component('Card', Card)

app.component('Toast', Toast)
app.component('DataTable', DataTable)
app.component('Column', Column)
app.component('ColumnGroup', ColumnGroup)

app.mount('#app');